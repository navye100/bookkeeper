//
//  ViewController.swift
//  Bookkeper
//
//  Created by VSY on 06/09/2018.
//  Copyright © 2018 VSY. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class ViewController: UIViewController, GeneralStyle {
    
    @IBOutlet weak var balance: UILabel!
    
    private let backColor: Int = 0xFFF176
    var incomeSum = 0.0
    var outlaySum = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackgroundColor(hexCode: backColor)
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func setBackgroundColor(hexCode: Int) {
        self.view.backgroundColor = UIColor.init(rgb: hexCode)
    }

    
    override func viewDidAppear(_ animated: Bool) {
        getSumIncome()
        print("resultIncome = ", incomeSum)
        getSumOutlay()
        print("resultOutlay = ", outlaySum)
    }
    
    func getSumIncome() {
        incomeSum = 0.0
        let query = PFQuery(className:"Transaction")
        query.whereKey("type", equalTo:"income")
    
        query.findObjectsInBackground {
            (objects, error) in

            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let objects = objects {
                    for object in objects {
                        let value  = object.object(forKey: "value")! as! Double
                        self.incomeSum = self.incomeSum + value
                        //print("incomeSum =", incomeSum)
                        print(object.object(forKey: "value")!)
                    }
                    print("incomeSum =", self.incomeSum)
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.localizedDescription)")
            }
            print("incomeSum =", self.incomeSum)
            self.getResult()
        }
    }
    
    func getSumOutlay() {
        outlaySum = 0.0
        let query = PFQuery(className:"Transaction")
        query.whereKey("type", equalTo:"outlay")
        
        query.findObjectsInBackground {
            (objects, error) in
            
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let objects = objects {
                    for object in objects {
                        let value  = object.object(forKey: "value")! as! Double
                        self.outlaySum = self.outlaySum + value
                        //print("incomeSum =", incomeSum)
                        print(object.object(forKey: "value")!)
                    }
                    print("outlaySum =", self.outlaySum)
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.localizedDescription)")
            }
            self.getResult()
        }
    }
    
    func getResult(){
        print("Result = ", incomeSum - outlaySum)
        balance.text = String.init(describing: incomeSum - outlaySum) 
    }
}
