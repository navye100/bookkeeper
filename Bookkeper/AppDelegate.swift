//
//  AppDelegate.swift
//  Bookkeper
//
//  Created by VSY on 06/09/2018.
//  Copyright © 2018 VSY. All rights reserved.
//

import UIKit
import Parse

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let config = ParseClientConfiguration {
            $0.applicationId = "xmZuz3JTR6ugT9ELTEPQFNXkMI3iq5AMHhZ2Mu2Z"
            $0.clientKey = "wQvwHfTxGU7CjVfIPvPy22lcjYMUxuX4Hfn214Vv"
            $0.server = "https://parseapi.back4app.com"
        }
        Parse.initialize(with: config)
        return true
    }
}

