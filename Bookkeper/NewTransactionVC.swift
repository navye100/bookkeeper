//
//  NewTransactionVC.swift
//  Bookkeper
//
//  Created by VSY on 07/09/2018.
//  Copyright © 2018 VSY. All rights reserved.
//

import UIKit
import Parse

class NewTransactionVC: UIViewController, GeneralStyle, UIPickerViewDelegate, UIPickerViewDataSource  {
    
    private let backColor: Int = 0x4FC3F7
    
    @IBOutlet weak var transactionType: UITextField!
    @IBOutlet weak var transactionDate: UITextField!
    @IBOutlet weak var transactionValue: UITextField!
    @IBOutlet weak var transactionNote: UITextField!
    
    
    private var dateFormat = DateFormatter()
    
    private var datePicker: UIDatePicker?
    private var typePicker: UIPickerView?
    private var transactionTypes = ["income", "outlay"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackgroundColor(hexCode: backColor)
        
        initTypePicker()
        initDatePicker()
    }
    
    func setBackgroundColor(hexCode: Int) {
        self.view.backgroundColor = UIColor.init(rgb: hexCode)
    }
    
    func initTypePicker() {
        typePicker = UIPickerView()
        typePicker?.delegate = self
        typePicker?.dataSource = self
        transactionType.text = transactionTypes[0]
        
        transactionType.inputView = typePicker
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
       transactionType.text = transactionTypes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return transactionTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return transactionTypes[row]
    }
    
    func initDatePicker() {
        dateFormat.dateFormat = "dd/MM/yyyy HH:mm"
        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.addTarget(self, action: #selector(NewTransactionVC.checkDateChanged(datePicker:)), for: .valueChanged)
        transactionDate.text = dateFormat.string(from: Date())
        transactionDate.inputView = datePicker
    }
    
    @objc func checkDateChanged(datePicker: UIDatePicker) {
        transactionDate.text = dateFormat.string(from: datePicker.date)
    }
    
    @IBAction func saveTransaction(_ sender: UIButton) {
        //let value: String = transactionValue.text!
        let transaction = PFObject(className: "Transaction")
        transaction["type"] = transactionType.text ?? ""
        transaction["date"] = transactionDate.text ?? ""
        transaction["value"] = Double(transactionValue.text!) ?? 0.0
        transaction["note"] = transactionNote.text ?? ""
        transaction.saveEventually()
        
        self.dismiss(animated: true, completion: nil)
        //_ = navigationController?.popViewController(animated: true)
        //performSegueToReturnBack()
    }
}
