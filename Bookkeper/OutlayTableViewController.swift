//
//  OutlayTableViewController.swift
//  Bookkeper
//
//  Created by VSY on 22/10/2018.
//  Copyright © 2018 VSY. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class OutlayTableViewController: PFQueryTableViewController, GeneralStyle {

    private let backColor: Int = 0xFF8A65
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackgroundColor(hexCode: backColor)
    }

    func setBackgroundColor(hexCode: Int) {
        self.view.backgroundColor = UIColor.init(rgb: hexCode)
    }
    
    override func queryForTable() -> PFQuery<PFObject> {
        let query = PFQuery(className: "Transaction")
        query.whereKey("type", equalTo: "outlay")
        //query.order(byAscending: "date")
        return query
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        let outlayCell = tableView.dequeueReusableCell(withIdentifier: "cellOutlay", for: indexPath) as! OutlayCell
        let valueNum = Double.init(exactly: object?.object(forKey: "value") as! Double)
        //print("value =", valueNum! )
        outlayCell.date.text = object?.object(forKey: "date") as? String
        outlayCell.note.text = object?.object(forKey: "note") as? String
        outlayCell.value.text = String(format:"%.1f", valueNum!)
        return outlayCell
    }

}
