//
//  TransactionStruct.swift
//  Bookkeper
//
//  Created by VSY on 11/09/2018.
//  Copyright © 2018 VSY. All rights reserved.
//

import Foundation

struct Transaction {
    var transType: String
    var transName: String
    var transDate: Int
    var transValue: Double
}
