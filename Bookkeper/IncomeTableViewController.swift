//
//  IncomeTableViewController.swift
//  Bookkeper
//
//  Created by VSY on 22/10/2018.
//  Copyright © 2018 VSY. All rights reserved.
//
import Foundation
import UIKit
import Parse
import ParseUI

class IncomeTableViewController: PFQueryTableViewController, GeneralStyle{
    
    private let backColor: Int = 0x81C784
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setBackgroundColor(hexCode: backColor)
    }
    
    func setBackgroundColor(hexCode: Int) {
        self.view.backgroundColor = UIColor.init(rgb: hexCode)
    }

    override func queryForTable() -> PFQuery<PFObject> {
        let query = PFQuery(className: "Transaction")
        query.whereKey("type", equalTo: "income")
        
        //query.order(byAscending: "date")
        return query
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell? {
        let incomeCell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TransCell
        let valueNum = Double.init(exactly: object?.object(forKey: "value") as! Double)
        //print("value =", valueNum!)
        incomeCell.date.text = object?.object(forKey: "date") as? String
        incomeCell.note.text = object?.object(forKey: "note") as? String
        incomeCell.value.text = String(format:"%.1f", valueNum!)
        return incomeCell 
    }
    
}
