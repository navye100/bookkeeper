//
//  GeneralStyleP.swift
//  Bookkeper
//
//  Created by VSY on 11/09/2018.
//  Copyright © 2018 VSY. All rights reserved.
//

import Foundation

protocol GeneralStyle {
    func setBackgroundColor(hexCode: Int)
}
