//
//  OutlayCell.swift
//  Bookkeper
//
//  Created by VSY on 22/10/2018.
//  Copyright © 2018 VSY. All rights reserved.
//

import Foundation
import UIKit
import Parse
import ParseUI

class OutlayCell: PFTableViewCell {
    
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var note: UILabel!
    @IBOutlet weak var value: UILabel!
}
