//
//  SettingsVC.swift
//  Bookkeper
//
//  Created by VSY on 07/09/2018.
//  Copyright © 2018 VSY. All rights reserved.
//

import UIKit
import Parse
import ParseUI


class IncomeVC: UIViewController, UITableViewDataSource, GeneralStyle{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return income.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = income[indexPath.row]
        return cell
    }
    
    var income = ["dsfd", "dfasdf"]
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
        //tabView.dataSource = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("viewWillAppear")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidAppear")
        getAllIncomes()
    }
    
    func setBackgroundColor(hexCode: Int) {
        
    }
    
    func getAllIncomes(){
        let query = PFQuery(className:"Transaction")
        query.whereKey("type", equalTo:"income")
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                // The find succeeded.
                print("Successfully retrieved \(objects!.count) scores.")
                // Do something with the found objects
                if let objects = objects {
                    for object in objects {
                        let value: Double = object.value(forKey: "value") as! Double
                        let date2: String = object.value(forKey: "date") as! String
                        print("value", value)
                        //self.date.text = date2
                        //self.income[objects.count] = date
                    }
                }
            } else {
                // Log details of the failure
                print("Error: \(error!) \(error!.localizedDescription)")
            }
        }
    }

}
