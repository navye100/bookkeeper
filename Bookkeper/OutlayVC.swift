//
//  OutlayVC.swift
//  Bookkeper
//
//  Created by VSY on 11/09/2018.
//  Copyright © 2018 VSY. All rights reserved.
//

import UIKit

class OutlayVC: UIViewController, GeneralStyle {
    
    private let backColor: Int = 0xFF8A65

    override func viewDidLoad() {
        super.viewDidLoad()
        setBackgroundColor(hexCode: backColor)
    }
    
    func setBackgroundColor(hexCode: Int) {
        self.view.backgroundColor = UIColor.init(rgb: hexCode)
    }
}
